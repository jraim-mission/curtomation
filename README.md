# Overview

This is some ghetto automation. You will need to change the python to match your usage. You have been warned.

Items in python that can be changed:
* `REPOS_BASE_DIR` - Name of the directory where the repos are downloaded. Default is `repos` in the main repository directory.
* `MASTER_PAYER_LIST_FILENAME` - The name of the CSV containing the payer account details - default is `master_payer_list.csv`
* `ASSIGNEE` - The key for the automation on which payers to run.

## Set Up

1. Install dependencies: [poetry](https://python-poetry.org/), [glab](https://docs.gitlab.com/ee/integration/glab/)
2. `poetry update`
3. `glab auth login`
4. Make sure your SSH key is correctly working with git and Gitlab
5. Create the repos directory - `mkdir repos`
6. Download the [repos list](https://docs.google.com/spreadsheets/d/1q2A9xYBSWPVLQG5s5UOR7zmLX3ZwunQnLOXSterHZ0I/edit?usp=sharing) as CSV and call it `master_payer_list.csv`


## Usage

The automation works by looking at the `master_payer-list.csv` file and parsing each line. It
then only runs lines that meet the following criteria:

* Assignee matches `ASSIGNEE` - e.g. lines assigned to you.
* Reviewer is not `done`. Anything marked as `done` will be skipped.
* Status must be in `Ready to Start`. Anything else will be skipped.

The automation will skip any line where the Core Respository is blank.

To perform a run, change the assignee of the line(s) that you want to run to match your setting
for `ASSIGNEE` and make sure the reviewer is blank.

Once you have the lines identified, run the command my just running `python main.py`. 



