import csv
import fileinput
import re
import subprocess

from packaging import version
from pathlib import Path

MASTER_PAYER_LIST_FILENAME = "master_payer_list.csv"
REPOS_BASE_DIR = "repos/"
VERSION_REGEX = '(\d+\.)?(\d+\.)?(\*|\d+)'
BRANCH_NAME = "add_monthly_cur"
COMMIT_MESSAGE = "Adding Monthly CUR"
ASSIGNEE = 'jraim'

payers_master_list = []

UPDATED_VERSIONS = {
    'mission-onboarding': '1.9.2',
    'payer-core': '2.5.0',
    'mission-cloudtrail-logging': '1.5.4',
    'cloudhealth': '1.5.1',
    'parquantix': '1.2.2',
    'payer-org-to-slack': '1.1.1'
}


def process_payer(payer_line: dict):
    print(f"{payer_line['Okta Label']} - PROCESSING")

    # Get the repository name / directory name
    repo_name = payer_line['Core repository'].split('/')[-1]

    # Looks like a bunch of repos were moved from customer to internal.
    internal_repo_url = payer_line['Core repository']

    print(f"\tCloning {payer_line['Okta Label']} - {internal_repo_url}")

    try:
        subprocess.run(['glab', 'repo', 'clone', internal_repo_url],
                       cwd=REPOS_BASE_DIR, capture_output=True, check=True)
    except subprocess.CalledProcessError as err:
        print(f"\tFailed to clone, trying again with internal instead of customer")
        internal_repo_url = payer_line['Core repository'].replace('customer', 'internal')

        try:
            subprocess.run(['glab', 'repo', 'clone', internal_repo_url],
                           cwd=REPOS_BASE_DIR, capture_output=True, check=True)
        except subprocess.CalledProcessError as err:
            print(f"\t Still failed to clone - bailing.")
            print(f"\t{err}")
            print(f"\t{err.stderr}")
            return False

    try:
        subprocess.run(['git', 'checkout', '-b', BRANCH_NAME],
                       cwd=f"{REPOS_BASE_DIR}{repo_name}", capture_output=True, check=True)
    except subprocess.CalledProcessError as err:
        print(f"\t Failed to create branch.")
        print(f"\t{err}")
        print(f"\t{err.stderr}")
        return False

    print("\tCompleted git clone")

    main_contents = Path(f"{REPOS_BASE_DIR}{repo_name}/main.tf").read_text()
    # Check if we can do this one
    check_string = "mission-onboarding/v"
    check_loc = main_contents.find(check_string)
    onboarding_version = main_contents[check_loc + len(check_string): check_loc + len(check_string) + 3]

    updated_versions = UPDATED_VERSIONS

    # Current focus is on 1.7 - 1.8
    if version.parse(onboarding_version) == version.parse("1.9.2"):
        print(f"\tSKIPPED - mission-onboarding version ({onboarding_version}) is 1.9.2 (current)")
        return False
    elif version.parse(onboarding_version) < version.parse("1.7"):
        # Don't replace mission-onboarding
        updated_versions.pop('mission-onboarding', None)
        print(f"\tmission-onboarding version ({onboarding_version}) is < 1.7. Only update other modules.")
    else:
        print(f"\tmission-onboarding version is in valid range ({onboarding_version}). Update all modules.")

    # Replace the versions
    for module in updated_versions:
        regex = f"{module}\/v{VERSION_REGEX}\/"
        main_contents = re.sub(regex, f"{module}/v{updated_versions[module]}/", main_contents)

    # Replace the enable_cur with enable_cur_hourly, but only in the payer-core
    payer_start = main_contents.find('module "payer-core"')
    payer_end = main_contents.find('module "', payer_start+1)
    payer_module = main_contents[payer_start:payer_end]

    payer_module = payer_module.replace("enable_cur", "enable_cur_hourly")
    main_contents = main_contents[:payer_start] + payer_module + main_contents[payer_end:]

    # Write out the updated main.tf
    with open(f"{REPOS_BASE_DIR}{repo_name}/main.tf", "w") as file:
        file.write(main_contents)

    # Update the providers
    with fileinput.FileInput(f"{REPOS_BASE_DIR}{repo_name}/providers.tf", inplace=True) as f:
        for line in f:
            if line.strip().startswith("version"):
                print('  version = "~> 3.0"')
            else:
                print(line, end='')

    print(f"\tFiles updated.")

    # Commit, check and push
    try:
        subprocess.run(['git', 'add', 'main.tf', 'providers.tf'],
                       cwd=f"{REPOS_BASE_DIR}{repo_name}", capture_output=True, check=True)
        subprocess.run(['git', 'commit', '-m', COMMIT_MESSAGE],
                       cwd=f"{REPOS_BASE_DIR}{repo_name}", capture_output=True, check=True)
        subprocess.run(['git', 'push', 'origin', BRANCH_NAME],
                       cwd=f"{REPOS_BASE_DIR}{repo_name}", capture_output=True, check=True)
        subprocess.run(['glab', 'mr', 'create', '--fill', '--remove-source-branch', '--yes'],
                       cwd=f"{REPOS_BASE_DIR}{repo_name}", capture_output=True, check=True)
    except subprocess.CalledProcessError as err:
        print(err)
        print(err.output)
        print(err.stderr)
        return False

    print(f"\tCommit pushed and MR created.")

    return True


if __name__ == '__main__':
    # Read the payers master file
    with open(MASTER_PAYER_LIST_FILENAME) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            payers_master_list.append(row)

    print(f"Total payers: {len(payers_master_list)} ")

    # Main loop
    for payer_line in payers_master_list:
        if payer_line['Assignee'] != ASSIGNEE:
            continue
        elif payer_line['Reviewer'] == 'done':
            continue
        elif payer_line['Status'] != 'Ready to Start':
            continue
        elif payer_line['Core repository'].strip() == '':
            print(f"{payer_line['Okta Label']} SKIPPED - core repository link is blank")
        else:
            if process_payer(payer_line):
                print(f"{payer_line['Okta Label']} - Success")
            else:
                print(f"{payer_line['Okta Label']} - *** FAILED *** ")
